﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using Universal.Access.Context;
using Universal.Access.Entities;
using Universal.TestFramework;
using Universal.TestFramework.Connectors;

namespace Satellite.Syndication.Test
{
    [TestFixture]
    public class SyndicationConnectorTest : ConfiguredPlatformTestBase
    {
        [SetUp]
        public void SetUp()
        {
            ZoneEntity = TestActions.CreateZone();

            IExtension connectorExtension = TestActions.CheckForExtension(SyndicationConnector.ExtensionGuid,
                                                                          typeof (SyndicationConnector));

            if (connectorExtension == null)
            {
                throw new Exception("Unable to locate connector extension!");
                return;
            }

            var parameters = new Dictionary<string, string>
                {
                    // No Parameters!
                };

            ConnectorEntity = TestActions.CreateConnector("Test Syndication Connector",
                                                          SyndicationConnector.ExtensionGuid, ZoneEntity, null,
                                                          parameters);
        }

        private const string ItemCount = "ITEM_COUNT";

        public IOrganisation OrganisationEntity { get; set; }
        public IZone ZoneEntity { get; set; }
        public IDivision DivisionEntity { get; set; }
        public IConnector ConnectorEntity { get; set; }

        [Test]
        public void GetFeedTest()
        {
            var parameters = new Dictionary<string, string>
                {
                    // Should always produce a result (c;
                    {"SYNDICATION_URL", "http://feeds.bbci.co.uk/news/rss.xml"}
                };

            ConnectorMethodResult results = TestActions.RunConnectorMethod(ConnectorEntity, "GetFeed", parameters);

            IDataContext context = results.Context;

            object status = context.GetResult(ReservedKeys.Status);
            object data = context.GetResult(ReservedKeys.Data);
            int count = Convert.ToInt32(context.GetResult(ItemCount));
            Assert.True(count > 0);
        }
    }
}