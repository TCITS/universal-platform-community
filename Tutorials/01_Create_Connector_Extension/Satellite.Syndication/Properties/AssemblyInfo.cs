﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Tech Connect IT Solutions Pty Ltd">
// 	Copyright (c) Tech Connect IT Solutions Pty Ltd. All rights reserved.
// 
// 	This is unpublished proprietary source code of Tech Connect IT Solutions Pty Ltd.
// 	The copyright notice above does not evidence any actual or intended publication of such source code.
// </copyright>
// --------------------------------------------------------------------------------------------------------------------

using System.Reflection;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Satellite.Syndication")]
[assembly: AssemblyDescription("Syndication Satellite")]
[assembly: AssemblyCulture("")]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("415449d9-b388-4abd-aaf7-9cbbcd013051")]
