﻿using System.Linq;
using System.Net;
using System.ServiceModel.Syndication;
using System.Xml;
using Universal.Access;
using Universal.Access.Connectors;
using Universal.Access.Context;
using Universal.Access.Credentials;
using Universal.Access.Extensions;
using Universal.Access.Parameters;

namespace Satellite.Syndication
{
    [ConnectorExtension(ExtensionGuidString, "Syndication", "RSS and Atom Syndication")]
    [ExtensionParameter(Credentials, typeof (GuidString), "Feed Credentials", typeof (UserCredentialParameter))]
    public class SyndicationConnector : BaseConnector<SyndicationConnector>
    {
        private const string ExtensionGuidString = "9d90eb27-2990-4e80-87dd-6c2fe5baec4e";

        private const string SyndicationUrl = "SYNDICATION_URL";
        private const string Credentials = "CREDENTIALS_GUID";
        private const string ItemCount = "ITEM_COUNT";
        public static readonly GuidString ExtensionGuid = new GuidString(ExtensionGuidString);

        [ConnectorMethod("Get Feed")]
        [ConnectorMethodParameter(SyndicationUrl, typeof (string), "Feed Location", true)]
        [ConnectorMethodResult(ReservedKeys.Data, typeof (object), "Feed Items")]
        [ConnectorMethodResult(ItemCount, typeof (int), "Item Count")]
        public void GetFeed(IDataContext context)
        {
            string syndicationUrl = context.Arguments.GetValue(SyndicationUrl);
            GuidString credentialGuid = context.Arguments.GetValueAsGuid(Credentials);

            if (!string.IsNullOrWhiteSpace(syndicationUrl))
            {
                var settings = new XmlReaderSettings();

                if (!credentialGuid.IsEmpty)
                {
                    IExecutableUserCredential credential;
                    if (!ServiceRepository.UserCredential.TryGetExecutableUserCredential(credentialGuid, out credential))
                    {
                        context.LogError("Unable to access credential");
                        return;
                    }

                    ICredentials networkCredential;
                    if (!credential.TryGetNetworkCredential(out networkCredential))
                    {
                        context.LogError("Unable to create network credential");
                        return;
                    }

                    settings.XmlResolver = new XmlUrlResolver {Credentials = networkCredential};
                }

                context.LogInfo("Connecting to " + syndicationUrl);

                using (XmlReader reader = XmlReader.Create(syndicationUrl, settings))
                {
                    SyndicationFeed feed = SyndicationFeed.Load(reader);

                    if (feed != null)
                    {
                        context.SetResult(feed.Items, PropertyDataSerializer.Instance);
                        context.SetResult(ItemCount, feed.Items.Count());
                    }
                }
            }
        }
    }
}