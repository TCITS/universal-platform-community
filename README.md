#Welcome to Universal Platform Community#
This is the home for the Universal Platform SDK.  The code examples presented will help guide you in creating the various Extensions used though-out the platform.

Click here to access our documentation and tutorials: [SDK Documentation Home](https://tech-connect.atlassian.net/wiki/display/UPH/SDK)
## Quick start
- [Download the latest Universal Platform](https://store.universalplatform.net).
- [Install the Universal Platform](https://tech-connect.atlassian.net/wiki/display/UPH/Universal+Platform+Installation+Guide).
- Clone the repo: `git clone https://bitbucket.org/TCITS/universal-platform-community.git`.
- [Getting Started](https://tech-connect.atlassian.net/wiki/display/UPH/SDK+-+Getting+Started).
- [Build a Syndication Connector Extension](https://tech-connect.atlassian.net/wiki/display/UPH/Syndication+Connector+Extension).

##Bugs and feature requests
Have a bug or a feature request?  Head on over to our support page  [Support](http://support.tech-connect.com.au/).